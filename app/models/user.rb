class User < ApplicationRecord
has_secure_password
has_many :products
before_create :auth_token


def auth_token
	self.auth_token = SecureRandom.urlsafe_base64
end

end
